import unittest

import numpy as np

from app.addition import row_major_add, col_major_add, numpy_add


class TestSum(unittest.TestCase):

    def setUp(self):
        self.matrix_x_3by3 = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]

        self.matrix_y_3by3 = [
            [9, 8, 7],
            [6, 5, 4],
            [3, 2, 1]
        ]

        # result has to be a 3*3 matrix with value 10 for each element.
        self.matrix_expected_3by3 = [
            [10, 10, 10],
            [10, 10, 10],
            [10, 10, 10]
        ]

        self.matrix_x_4by5 = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
            [17, 18, 19, 20]
        ]

        self.matrix_y_4by5 = [
            [20, 19, 18, 17],
            [16, 15, 14, 13],
            [12, 11, 10, 9],
            [8, 7, 6, 5],
            [4, 3, 2, 1]
        ]

        # result has to be a 4*5 matrix with value 10 for each element.
        self.matrix_expected_4by5 = [
            [21, 21, 21, 21],
            [21, 21, 21, 21],
            [21, 21, 21, 21],
            [21, 21, 21, 21],
            [21, 21, 21, 21]
        ]

    def test_n_by_n_matrix_addition(self):
        result = row_major_add(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_big_n_by_n_matrix_addition(self):
        matrix_x = np.random.randint(999, size=(50, 50))
        matrix_y = np.random.randint(999, size=(50, 50))
        np_result = matrix_x + matrix_y
        result = row_major_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)

    def test_big_n_by_m_matrix_addition(self):
        matrix_x = np.random.randint(999, size=(50, 80))
        matrix_y = np.random.randint(999, size=(50, 80))
        np_result = matrix_x + matrix_y
        result = row_major_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)

    def test_m_by_n_matrix_addition(self):
        result = row_major_add(self.matrix_x_4by5, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_4by5, result)

    def test_n_by_n_matrix_addition_column_traversal(self):
        result = col_major_add(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_m_by_n_matrix_addition_column_traversal(self):
        result = col_major_add(self.matrix_x_4by5, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_4by5, result)

    def test_big_n_by_n_matrix_addition_column_traversal(self):
        matrix_x = np.random.randint(999, size=(25, 25))
        matrix_y = np.random.randint(999, size=(25, 25))
        np_result = matrix_x + matrix_y
        result = col_major_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)

    def test_big_n_by_m_matrix_addition_column_traversal(self):
        matrix_x = np.random.randint(999, size=(25, 30))
        matrix_y = np.random.randint(999, size=(25, 30))
        np_result = matrix_x + matrix_y
        result = col_major_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)

    def test_n_by_n_numpy_matrix_addition_column_traversal(self):
        result = numpy_add(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_m_by_n_numpy_matrix_addition_column_traversal(self):
        result = numpy_add(self.matrix_x_4by5, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_4by5, result)

    def test_big_n_by_n_numpy_matrix_addition_column_traversal(self):
        matrix_x = np.random.randint(999, size=(25, 25))
        matrix_y = np.random.randint(999, size=(25, 25))
        np_result = matrix_x + matrix_y
        result = numpy_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)

    def test_big_n_by_m_numpy_matrix_addition_column_traversal(self):
        matrix_x = np.random.randint(999, size=(25, 30))
        matrix_y = np.random.randint(999, size=(25, 30))
        np_result = matrix_x + matrix_y
        result = numpy_add(matrix_x, matrix_y)
        self.assertListEqual(np_result.tolist(), result)


if __name__ == '__main__':
    unittest.main()
