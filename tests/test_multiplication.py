import unittest

import numpy as np

from app.multiplication import (
    naive_multiply, naive_transpose_multiply,
    blocking_multiply,
    faster_blocking_multiply,
    faster_naive_multiply, numpy_multiply)


class TestMatrixMultiplication(unittest.TestCase):
    def setUp(self):
        self.matrix_x_3by3 = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]]

        self.matrix_y_3by3 = [
            [9, 8, 7],
            [6, 5, 4],
            [3, 2, 1]]

        self.matrix_expected_3by3 = [
            [30, 24, 18],
            [84, 69, 54],
            [138, 114, 90]]

        self.matrix_x_3by4 = [
            [1, 1, 1, 1],
            [2, 2, 2, 2],
            [3, 3, 3, 3]]

        self.matrix_y_4by5 = [
            [4, 4, 4, 4, 4],
            [5, 5, 5, 5, 5],
            [6, 6, 6, 6, 6],
            [7, 7, 7, 7, 7]]

        self.matrix_expected_3by5 = [
            [22, 22, 22, 22, 22],
            [44, 44, 44, 44, 44],
            [66, 66, 66, 66, 66]]

    def test_simple_multiply_nn_by_nn(self):
        result = naive_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_simple_multiply_nm_by_mk(self):
        result = naive_multiply(self.matrix_x_3by4, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_3by5, result)

    def test_simple_multiply_nn_by_nn_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = naive_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_simple_multiply_nm_by_mk_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 40))
        y = np.random.randint(999, size=(40, 30))
        np_result = x.dot(y)
        result = naive_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_simple_multiply_nn_by_nn_faster(self):
        result = faster_naive_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_simple_multiply_nn_by_nn_for_big_matrices_faster(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = faster_naive_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_naive_transpose_multiply_nn_by_nn(self):
        result = naive_transpose_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_naive_transpose_multiply_nm_by_mk(self):
        result = naive_transpose_multiply(self.matrix_x_3by4, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_3by5, result)

    def test_naive_transpose_multiply_nn_by_nn_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = naive_transpose_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_naive_transpose_multiply_nm_by_mk_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 40))
        y = np.random.randint(999, size=(40, 30))
        np_result = x.dot(y)
        result = naive_transpose_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_blocking_multiply_nn_by_nn(self):
        result = blocking_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_blocking_multiply_nn_by_nn_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = blocking_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_blocking_multiply_nn_by_nn_faster(self):
        result = faster_blocking_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_blocking_multiply_nn_by_nn_faster_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = faster_blocking_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_numpy_multiply_nn_by_nn_faster(self):
        result = numpy_multiply(self.matrix_x_3by3, self.matrix_y_3by3)
        self.assertListEqual(self.matrix_expected_3by3, result)

    def test_numpy_multiply_by_row_nm_by_mk_faster(self):
        result = numpy_multiply(self.matrix_x_3by4, self.matrix_y_4by5)
        self.assertListEqual(self.matrix_expected_3by5, result)

    def test_numpy_multiply_nn_by_nn_faster_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 25))
        y = np.random.randint(999, size=(25, 25))
        np_result = x.dot(y)
        result = numpy_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)

    def test_numpy_multiply_nm_by_mk_faster_for_big_matrices(self):
        x = np.random.randint(999, size=(25, 40))
        y = np.random.randint(999, size=(40, 30))
        np_result = x.dot(y)
        result = numpy_multiply(x, y)
        self.assertListEqual(np_result.tolist(), result)
