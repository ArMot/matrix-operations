import unittest

from app.utils import create_matrix, transpose, specify_block_size


class TestUtilityFunctions(unittest.TestCase):
    def test_create_matrix_with_given_row_and_column_size(self):
        """Test creating a matrix with given row and col size"""
        matrix = create_matrix(5, 10)
        self.assertEqual(len(matrix), 5)  # test row size
        [self.assertEqual(len(col), 10) for col in matrix]  # test every column size

    def test_transpose_n_by_n_matrix(self):
        """Test transposing an n*n matrix"""
        matrix = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]]

        expected = [
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9]]

        result = transpose(matrix)
        self.assertListEqual(expected, result)

    def test_transpose_n_by_m_matrix(self):
        """Test transposing an n*m matrix"""
        matrix = [
            [1, 2, 3, 4],
            [5, 6, 7, 8]]

        expected = [
            [1, 5],
            [2, 6],
            [3, 7],
            [4, 8]]

        result = transpose(matrix)
        self.assertListEqual(expected, result)

    def test_block_size(self):
        self.assertEqual(specify_block_size(6000), 250)
        self.assertEqual(specify_block_size(1500), 250)
        self.assertEqual(specify_block_size(1000), 100)
        self.assertEqual(specify_block_size(500), 100)
