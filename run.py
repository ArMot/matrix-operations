import csv
import os
import platform
import timeit
from functools import partial

import numpy as np
from matplotlib import pyplot

import app.addition as add
import app.multiplication as mul
from app.utils import create_matrix

OUTPUT_DIR = 'output'


def save_to_file(file: str, func_name: str, matrix_size: int, elapsed_time) -> None:
    """
    Log matrix size, function and time it took to compute to a csv file
    @type file: this is the log file name
    @type func_name: function name that is intended to log
    @type matrix_size: the size of matrix we made computation on
    @type elapsed_time: time it took for computation to complete. its in seconds.
    """
    with open(f'{OUTPUT_DIR}/{file}.csv', mode='a') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([func_name, matrix_size, matrix_size, elapsed_time])


def plot_time(func, size_list, repeats, n_tests, file):
    """
    Run timer and plot time complexity of `func` using the iterable `inputs`.
    Run the function `n_tests` times per `repeats`.
    """
    x, y, yerr = [], [], []
    for i in size_list:
        print(f'Calculating {func.__name__} for {i}x{i} matrix ...', end='|\t')
        try:
            matrix = create_matrix(i, i)
            timer = timeit.Timer(partial(func, matrix, matrix))
            t = timer.repeat(repeat=repeats, number=n_tests)
            print(f'it took {t} seconds')
        except:
            print(f'For {func.__name__} {i}*{i} matrix is the limit on this machine')
            pyplot.errorbar(x, y, yerr=yerr, fmt='-o', label=func.__name__)
            return

        del matrix
        x.append(i / 1000)  # If results get so big, they can not appear in plot.
        y.append(np.mean(t))
        yerr.append(np.std(t) / np.sqrt(len(t)))
        save_to_file(file, func.__name__, i, np.mean(t))
    pyplot.errorbar(x, y, yerr=yerr, fmt='-o', label=func.__name__)


def plot_times(functions, size_list, repeats=3, n_tests=1, file_name=""):
    """
    Run timer and plot time complexity of all `functions`,
    using the iterable `inputs`.
    Run the functions `n_tests` times per `repeats`.
    Adds a legend containing the labels added by `plot_time`.
    """

    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    for func in functions:
        plot_time(func, size_list, repeats, n_tests, file_name)

    pyplot.legend()
    pyplot.xlabel("Input [N*N matrix]")
    pyplot.ylabel("Time [s]")
    if not file_name:
        pyplot.show()
    else:
        pyplot.savefig(f'{OUTPUT_DIR}/{file_name}')


def decrease_process_niceness_value():
    """If user os is linux or unix and run the script using sudo command, the niceness value of
    process set to -15.
    this means that the process gets more cpu time and higher priority to execute (max is -20)"""
    user_os = platform.system()
    if (user_os.lower() == 'linux' or user_os.lower() == 'unix') and os.getenv('USER') == 'root':
        os.nice(-15)
        print('niceness set to -15. this means more CPU time, and more priority!')
        print('-----------------------------------------------------------')
    else:
        print('this process has normal priority and cpu time. if you want to change it priority '
              'and give it more CPU time, rerun this script using a <sudo> command.')
        print('-----------------------------------------------------------')


def log_only_single_function(func, size_list, repeats, file_name, n_tests=1):
    for i in size_list:
        try:
            print(f'(LOG ONLY) Calculating {func.__name__} for {i}x{i} matrix ...', end='|\t')
            matrix = create_matrix(i, i)
            timer = timeit.Timer(partial(func, matrix, matrix))
            t = timer.repeat(repeat=repeats, number=n_tests)
            print(f'it took {t} seconds')
        except:
            print(f'For {func.__name__} {i}*{i} matrix is the limit on this machine')
            return

        del matrix
        save_to_file(file_name, func.__name__, i, np.mean(t))


def ask_user_select_method():
    print('Which method you want to log (enter number 1-9): ')
    print('---------------------Addition-----------------------------------')
    print('1. Row-major addition')
    print('2. Col-major addition')
    print('3. Numpy default addition')
    print('---------------------Multiplication------------------------------')
    print('4. Naive method (IJK)')
    print('5. naive method (improved)')
    print('6. Transposed naive method')
    print('7. Blocking method')
    print('8. Blocking method (improved)')
    print('9. Numpy default multiplication')
    print()
    method = int(input('choice: '))
    if method == 1:
        return add.row_major_add
    elif method == 2:
        return add.col_major_add
    elif method == 3:
        return add.numpy_add
    elif method == 4:
        return mul.naive_multiply
    elif method == 5:
        return mul.faster_naive_multiply
    elif method == 6:
        return mul.naive_transpose_multiply
    elif method == 7:
        return mul.blocking_multiply
    elif method == 8:
        return mul.faster_blocking_multiply
    elif method == 9:
        return mul.numpy_multiply
    else:
        raise ValueError("Choices are obvious! You MUST pick a number between 1-9")


def get_user_inputs():
    result = dict()
    print('-----------------------------------------------------------')
    print('''
    Enter a number between 1-4:
    (1) for matrix addition comparison
    (2) for matrix multiplication comparison 
    (3) single function log only
    (4) plot time for previously logged methods
    '''
          )
    choice = int(input('Choice: ') or '1')
    if choice == 4:
        result['plot_only'] = True
        result['file_path'] = input('csv log location on your machine: ')
    else:
        result['choice'] = choice
        sizes_str = input('Enter sizes for matrix you want to process, separated by comma (for '
                          'example: 500, 1000 means 500x500, 1000x1000): ')
        result['repeats'] = int(
            input('Number of repeats per each test (higher numbers results to a '
                  'more '
                  'accurate result but highly increases time (default is 3): ') or '3')
        result['file_name'] = input('Enter a file name so the result will be saved with this '
                                    'name: ')
        print('-----------------------------------------------------------')
        sizes_str = sizes_str.replace(' ', '')
        sizes_str = sizes_str.split(',')
        sizes = []
        for item in sizes_str:
            sizes.append(int(item))
        result['size'] = sizes

    if choice == 1:
        result['funcs'] = [add.row_major_add, add.col_major_add, add.numpy_add]
    elif choice == 2:
        result['funcs'] = [
            mul.naive_multiply, mul.faster_naive_multiply,
            mul.naive_transpose_multiply,
            mul.blocking_multiply, mul.faster_blocking_multiply, mul.numpy_multiply]
    elif choice == 3:
        result['log_only'] = True
        result['log_func'] = ask_user_select_method()

    return result


def plot_time_from_logs(file_name):
    data = extract_data_from_logs(file_name)

    for func in data:
        plot_single_log_item(func, data[func])

    pyplot.legend()
    pyplot.xlabel("Input [N*N matrix]")
    pyplot.ylabel("Time [s]")
    name = os.path.basename(file_name)
    name = os.path.splitext(name)[0]
    pyplot.savefig(f'{OUTPUT_DIR}/{name}')


def plot_single_log_item(func, values):
    x, y, yerr = [], [], []
    for value in values:
        size = value['size_x']
        time = value['time'] / 3600
        x.append(size)
        y.append(time)
        yerr.append(time)
    pyplot.errorbar(x, y, yerr=yerr, fmt='-o', label=func)


def extract_data_from_logs(file_name):
    with open(file_name, 'r+') as log_file:
        reader = csv.reader(log_file, delimiter=',')
        data = dict()
        for row in reader:
            if data.get(row[0], None) is None:
                data[row[0]] = []
            func_info = {'size_x': int(row[1]), 'size_y': int(row[2]), 'time': float(row[3])}
            data[row[0]].append(func_info)
    return data


if __name__ == "__main__":
    decrease_process_niceness_value()

    user_input = get_user_inputs()
    if user_input.get('log_only', False):
        log_only_single_function(
            user_input['log_func'],
            user_input['size'],
            repeats=user_input['repeats'],
            file_name=user_input['file_name'])
    elif user_input.get('plot_only', False):
        plot_time_from_logs(user_input['file_path'])
    else:
        plot_times(
            user_input['funcs'],
            user_input['size'],
            repeats=user_input['repeats'],
            file_name=user_input['file_name'])
