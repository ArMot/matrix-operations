from app.utils import create_zero_matrix, specify_block_size, transpose

import numpy as np


def naive_multiply(first, second):
    rows, cols = len(first), len(second[0])
    result = create_zero_matrix(rows, cols)

    for i in range(len(first)):
        for j in range(len(second[0])):
            for k in range(len(second)):
                result[i][j] += first[i][k] * second[k][j]

    return result


def faster_naive_multiply(first, second):
    """
    same as baseline but switch j and k loops
    """
    rows = len(first)
    cols = len(second[0])
    result = create_zero_matrix(rows, cols)
    for i in range(len(first)):
        ci = result[i]
        for k in range(len(second[0])):
            bk = second[k]
            aik = first[i][k]
            for j in range(len(second)):
                ci[j] += aik * bk[j]
    return result


def naive_transpose_multiply(first, second):
    rows, cols = len(first), len(second[0])
    result = create_zero_matrix(rows, cols)
    t_second = transpose(second)
    del second

    for i in range(len(first)):
        for j in range(len(t_second)):
            for k in range(len(t_second[0])):
                result[i][j] += first[i][k] * t_second[j][k]

    return result


def blocking_multiply(first, second):
    """
    use blocking
    """
    # NM * MK matrix multiplication result is a NK matrix
    rows = len(first)
    cols = len(second[0])
    result = create_zero_matrix(rows, cols)

    block_row_size = specify_block_size(len(first))
    block_col_size = specify_block_size(len(second[0]))

    for row in range(0, rows, block_row_size):
        for col in range(0, cols, block_col_size):
            for block_row in range(row, row + block_row_size):
                for block_col in range(col, col + block_col_size):
                    sum = result[block_row][block_col]
                    for dot in range(rows):
                        sum += first[block_row][dot] * second[dot][block_col]
                    result[block_row][block_col] = sum
    return result


def faster_blocking_multiply(first, second):
    """
    use blocking
    """
    # NM * MK matrix multiplication result is a NK matrix
    rows = len(first)
    cols = len(second[0])
    # result = np.zeros((rows, cols), int)
    result = [[0 for col in range(len(second[0]))] for row in range(len(first))]

    block_row_size = specify_block_size(len(first))
    block_col_size = specify_block_size(len(second[0]))

    for row in range(0, rows, block_row_size):
        for col in range(0, cols, block_col_size):
            for block_row in range(row, row + block_row_size):
                cache_first = first[block_row]
                cache_result = result[block_row]
                for block_col in range(col, col + block_col_size):
                    sum = cache_result[block_col]
                    for dot in range(rows):
                        sum += cache_first[dot] * second[dot][block_col]
                    cache_result[block_col] = sum
    return result


def numpy_multiply(first, second):
    np_first = np.asarray(first)
    np_second = np.asarray(second)
    del first, second
    result = np_first.dot(np_second)
    return result.tolist()
