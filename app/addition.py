from app.utils import transpose, transpose

import numpy as np


def row_major_add(first, second):
    rows, cols = len(first), len(first[0])
    # result = [[0] * cols] * rows
    result = []
    for row in range(rows):
        result_col = [0] * cols
        for col in range(cols):
            result_col[col] = first[row][col] + second[row][col]
        result.append(result_col)

    return result


def col_major_add(first, second):
    rows, cols = len(first), len(first[0])
    result = []
    # result = [[0] * cols] * rows

    for col in range(cols):
        result_row = [0] * rows
        for row in range(rows):
            result_row[row] = first[row][col] + second[row][col]
        result.append(result_row)
    return transpose(result)


def numpy_add(first, second):
    np_first = np.asarray(first)
    np_second = np.asarray(second)
    del first, second
    result = np_first + np_second
    return result.tolist()
