import random


def create_matrix(rows: int, cols: int):
    """ Create a random matrix with given row and column size"""
    matrix = []
    for i in range(rows):
        sample_list = random.choices(range(999), weights=None, k=cols)
        matrix.append(sample_list)

    return matrix


def create_zero_matrix(row_size, col_size):
    return [[0 for _ in range(col_size)] for _ in range(row_size)]


def transpose(matrix):
    """Transposing matrix and return the result as a new matrix"""
    row, col = (len(matrix), len(matrix[0]))
    result = []

    t_matrix = zip(*matrix)
    for row in t_matrix:
        result.append(list(row))

    return list(result)


def specify_block_size(size: int) -> int:
    """Specify the block size that we use for blocking method in matrix multiplications"""
    if size % 250 == 0 and size // 250 >= 5:
        return 250
    elif size % 100 == 0 and size // 100 >= 5:
        return 100

    for i in reversed(range(size // 2, 1)):
        if size % i == 0:
            return i

    return size  # size is probably a prime number!
