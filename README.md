# Matrix Operations
_**This project's goal is to compare different matrix addition and multiplication  algorithms.
this app is able to execute each method, log the information and plot the time complexity for all 
methods. this way you can have a great comparison using just a few clicks.**_

## Implemented Algorithms for addition:
1. Traversing matrix by rows and adding the corresponding elements in the matrices.
2. Traversing matrix by columns and adding the corresponding elements in the matrices.
3. Adding two matrices using numpy library

## Implemented algorithms for multiplication:
1. The naive algorithm (IJK)
2. The naive algorithm (improved by caching elements)
3. The transposed naive and multiply
4. Blocking algorithm
5. Blocking algorithm (improved by caching elements)
6. Numpy matrix multiply method (dot product)

## How to run the app.
### Install dependencies
make sure _Virtualenv_ or _Pipenv_ is installed on your machine.
create a virtual environment using

`$virtualenv venv`

then activate it by following command:

`$source venv/bin/activate`

You MUST install dependencies to use the app.

`pip install -r requirements.txt`

### Run tests to make sure everything works
to run unittests you need to install pytest. which is installed by requirements.txt (if you 
followed this readme file). then use this command at project root directory to run the tests:

`python -m pytest`

if all the test passes, you are good to go!

### Using the app
you can run the app as a regular user or as a sudo user.
`$python app/run.py
`
or
`$sudo python app/run.py
`.

by using sudo command. the app can change **niceness** of the process. this way you give it a _high 
priority_ which means more _CPU time_!